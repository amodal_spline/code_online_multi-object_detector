
  Online Multi-Object Detection

  Description:
    This file contains the code to perform online learning and detection of
    multiple objects using minimal human supervision [1]. More specifically, this
    code computes multiple online classifiers to learn and detect simultaneously
    various objects using human assistance. The degree of human intervention is 
    reduced progressively in accordance to the classifier performance. Initially, 
    the human selects via the computer's mouse the object in the image that he/she 
    wants to learn and recognize in future frames. Subsequently, each classifier is 
    initially computed using a set of training samples generated artificially. 
    Positive samples are extracted using random shift transformations over the 
    object image, whereas negative samples are random patches from the background.

    In run time, each classifier detects all possible object instances in the image. 
    The classifiers use their own detection predictions and the human assistance to 
    update and refine the object appearance models (interactive learning). Precisely, 
    the human assistance provides the true sample label (class) in those cases where
    the classifier is uncertain abuout its detection predictions (difficult samples).
    
    If you make use of this code, we kindly encourage to cite the reference [1], 
    listed below. This code is only for research and educational purposes.

  Requirements:
    1. opencv (e.g opencv 2.4.9)
    2. cmake

  Compilation:
    1. mkdir build
    2. cd build
    3. cmake ..
    3. make
    4a. ./detector (for webcam)
    4b. ./detector ../videos/video.avi (for input video file)
    
  Comments:
    1. The program works at any input image resolution. However, the screen
       information like score, results, etc, are displayed for a resolution
       of 640x480 pixels. This is the default resolution. If you change the
       resolution you must sure that the functions are shown properly. We
       recommend not using a resolution lower of 640x480 pixels.
    2. The program parameters are defined in the file parameters.txt located
       in the files folder. In this file you can change the detector parameters
       and program funcionalities.

  Keyboard commands:
    ESC: exit the program
    space: enable/disable object detection
    v: change the visualization mode (three modes are considered in the program)
    k: change the classifier index to show the learning results on the screen
    
  Contact:
    Michael Villamizar
    mvillami@iri.upc.edu
    Institut de Robòtica i Informàtica Industrial, CSIC-UPC
    Barcelona - Spain
    2015

  References:
    [1] Modeling Robot's World with Minimal Effort
        M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer
        International Conference on Robotics and Automation (ICRA)
        Seattle, USA, July 2015 



